﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFisrt.Models
{
    public class Person
    {
        public Person()
        {
        }

        public Person(string theName)
        {
            Name = theName;
        }

        public string Name { get; set; }
        public int? Age { get; set; }
        public void AddAge(string value) 
        {
            if (int.TryParse(value, out int age))
            {
                Age = age;
            } else {
                Age = null;
            }
        }

        public void PrintDetails() 
        {
            int theAgeMain = 0;
            if (Age.HasValue)
            {
                theAgeMain = Age.Value;
            }
            Console.WriteLine($"{Name} So U are {theAgeMain}");
        }
    }
}
