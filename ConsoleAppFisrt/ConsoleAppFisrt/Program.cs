﻿using ConsoleAppFisrt.Models;
using ConsoleAppFisrt.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFisrt
{
    class Program
    {
        static void Main(string[] args)
        {
            while (HandleFlow.ShouldIContinue())
            {
                UserMessages userCalls = new UserMessages();
                userCalls.WelcomeUser();
                userCalls.AskAgeByUser(new Person(userCalls.AskNameByUser())).PrintDetails();
                HandleFlow.CheckAndChangeStatus();
            }
            HandleFlow.SayGoodbye();
        }
    }
}
