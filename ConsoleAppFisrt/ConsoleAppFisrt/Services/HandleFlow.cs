﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFisrt.Services
{
    public static class HandleFlow
    {
        private static bool _toContinue = true;
        public static void CheckAndChangeStatus() 
        {
            if (Console.ReadLine().ToLower() == "stop")
            {
                _toContinue = false;
            }
        }

        public static bool ShouldIContinue() 
        {
            return _toContinue;
        }

        public static void SayGoodbye() 
        {
            Console.WriteLine("Bye !!!");
            Console.ReadLine();
        } 

    }
}
