﻿using ConsoleAppFisrt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppFisrt.Services
{
    public class UserMessages
    {
        public void WelcomeUser() 
        {
            Console.WriteLine($"HI THERE!!!");
        }

        public string AskNameByUser()
        {
            Console.WriteLine($"Please Type Your Name");
            return Console.ReadLine();
        }

        public Person AskAgeByUser(Person person)
        {
            Console.WriteLine($"Please Type Your Age {person.Name}");
            string val = Console.ReadLine();
            person.AddAge(val);
            return person;
        }
    }
}
